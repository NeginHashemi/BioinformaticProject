
# Libraries -------------------------------------------------------------

# use "Biostrings" to read fasta format files 
library(Biostrings) 
library(R.oo)
library(ape) # nj
library(phangorn) # upgma
library(lineup) # combinedist


# Global Variables --------------------------------------------------------

score.matrix <- nucleotideSubstitutionMatrix(match = 1, mismatch = -1, baseOnly = TRUE) 
gap <- -1
names_viruses <- c("Bundibugyo", "TaiForest", "Reston", "Sudan", "Zaire", "Marburg")

# Functions ---------------------------------------------------------------

# Read .fasta file in given path and return string in given index.
Get_Data_EBOV <- function(path){
  genome <- readDNAStringSet(path)
  seq_name = names(genome)
  sequence = paste(genome)
  df <- data.frame(seq_name, sequence) 
  return(toString(df[1,2]))
}
# Create edit distance matrix for given genes and store in file 
Create_Matrix <- function(Gene, i, j){
  a <- 2;
  b <- 6;
  if(!missing(i) || !missing(j)) {
    a <- i;
    b <- j;
  } 
  k <- b - a + 1
  paired_sequences <- expand.grid(Gene[a:b], Gene[a:b]) 
  gene_matrix <- matrix(nedit(pairwiseAlignment(paired_sequences$Var1, paired_sequences$Var2,
                                             substitutionMatrix = score.matrix, gapOpening = 1, gapExtension = 1)),
                     nrow = k)
   
  c <- a - 1
  d <- b - 1
  rownames(gene_matrix) <- names_viruses[c:d]
  colnames(gene_matrix) <- names_viruses[c:d]
  write.table(gene_matrix, file = paste("edit_distances\\", Gene[1], ".csv"), col.names = TRUE, row.names = TRUE,sep="\t")
  return(gene_matrix)
}
# Plot trees and store each tree in .jpg file
Create_Trees <- function(MT, name){
  dev.copy(png, paste('trees\\UPGMA', name, '.png', sep = ""))
  plot(upgma(MT))
  dev.off()
  dev.copy(png, paste('trees\\NJ', name, '.png', sep = ""))
  plot(nj(MT))
  dev.off()
  # print(paste("Is_Additive : ", Is_Additive(MT)))
  # print(paste("Is_Ultrametric : ", Is_Ultrametric(MT)))
}

# Check for additive matrix
Is_Additive <- function(MT){
  n <- nrow(MT) # Should be equal to ncol(MT)
  res <- TRUE
  for (i in 1:n) {
    for (j in i:n) {
      for (k in j:n) {
        for (p in k:n) {
          if(MT[i,j] + MT[k,p] > max(MT[i,k] + MT[j, p], MT[i, p] + MT[j, k])){
            res <- FALSE           
          }
        }
      }
    }
  }
  return(res)
}

# Check for ultrametric(triangular) matrix
Is_Ultrametric <- function(MT){
  n <- nrow(MT) # Should be equal to ncol(MT)
  res <- TRUE
  for (i in 1:n) {
    for (j in i:n) {
      for (k in j:n) {
          if(MT[i,j] > MT[i,k] + MT[j, k] && res){
            res <- FALSE
            print(MT[i,j])
            print(MT[i,k] + MT[j, k])
          }
      }
    }
  }
  return(res)
}

# Get matrix with "lineupdist" class
Lineupdist <- function(MT){
  MT <- as.matrix(MT) 
  class(MT) <- "lineupdist"
  return(MT)
}

# Return aligned strings
Aligned_Strings <- function(Gene, i, j){
  a <- 2;
  b <- 6;
  if(!missing(i) || !missing(j)) {
    a <- i;
    b <- j;
  } 
  k <- b - a + 1
  paired_sequences <- expand.grid(Gene[a:b], Gene[a:b]) 
  n <- subject(pairwiseAlignment(paired_sequences$Var1, paired_sequences$Var2,
                                                substitutionMatrix = score.matrix))
                        
  print(summary(n))
  return(n)
}

# Load data ---------------------------------------------------------------

path <- "gene\\"

NP <- "NP"
for (i in 1:5) {
  NP <- c(NP, Get_Data_EBOV(paste(path, names_viruses[i], "_gene_", toString(1), ".fasta", sep = "")))
}

VP35 <- "VP35"
for (i in 1:5) {
  VP35 <- c(VP35, Get_Data_EBOV(paste(path, names_viruses[i], "_gene_", toString(2), ".fasta", sep = "")))
}

VP40 <- "VP40"
for (i in 1:5) {
  VP40 <- c(VP40, Get_Data_EBOV(paste(path, names_viruses[i], "_gene_", toString(3), ".fasta", sep = "")))
}

GP <- "GP"
for (i in 1:5) {
  GP <- c(GP, Get_Data_EBOV(paste(path, names_viruses[i], "_gene_", toString(4), ".fasta", sep = "")))
}

VP30 <- "VP30"
for (i in 1:5) {
  VP30 <- c(VP30, Get_Data_EBOV(paste(path, names_viruses[i], "_gene_", toString(5), ".fasta", sep = "")))
}

VP24 <- "VP24"
for (i in 1:5) {
  VP24 <- c(VP24, Get_Data_EBOV(paste(path, names_viruses[i], "_gene_", toString(6), ".fasta", sep = "")))
}

L <- "L"
for (i in 1:5) {
  L <- c(L, Get_Data_EBOV(paste(path, names_viruses[i], "_gene_", toString(7), ".fasta", sep = "")))
}
genomes_matrix <- "GENOMES"
Bundibugyo <- Get_Data_EBOV("Bundibugyo_genome.fasta")
TaiForest <- Get_Data_EBOV("TaiForest_genome.fasta")
Reston <- Get_Data_EBOV("Reston_genome.fasta")
Sudan <- Get_Data_EBOV("Sudan_genome.fasta")
Zaire <- Get_Data_EBOV("Zaire_genome.fasta")
genomes_matrix <- c(genomes_matrix, Bundibugyo, TaiForest, Reston, Sudan, Zaire)


# Align -----------------------------------------------------------------

NP_M <- Create_Matrix(NP, 2, 6) 
VP35_M <- Create_Matrix(VP35, 2, 6)
VP40_M <- Create_Matrix(VP40, 2, 6)
GP_M <- Create_Matrix(GP, 2, 6)
VP30_M <- Create_Matrix(VP30, 2, 6)
VP24_M <- Create_Matrix(VP24, 2, 6)
L_M <- Create_Matrix(L, 2, 6)


# UPGMA or NJ -------------------------------------------------------------------

# UPGMA is preferred because matrices are not additive and are ultrametric. Also UPGMA is rooted.
Create_Trees(NP_M, "NP")
Create_Trees(VP35_M, "VP35")
Create_Trees(VP40_M, "VP40")
Create_Trees(GP_M, "GP")
Create_Trees(VP30_M, "VP30")
Create_Trees(VP24_M, "VP24")
Create_Trees(L_M, "L")


# Combine trees -----------------------------------------------------------

NP_M <- Lineupdist(NP_M)
VP35_M <- Lineupdist(VP35_M)
VP24_M <- Lineupdist(VP24_M)
VP40_M <- Lineupdist(VP40_M)
VP30_M <- Lineupdist(VP30_M)
GP_M <- Lineupdist(GP_M)
L_M <- Lineupdist(L_M)

combined_consensus <- consensus(upgma(NP_M), upgma(VP35_M), upgma(VP40_M), 
                 upgma(GP_M), upgma(VP30_M), upgma(VP24_M), upgma(L_M),  p = 0.5)

combined_mean <- combinedist(NP_M, VP35_M, VP40_M, GP_M, VP30_M, VP24_M, L_M,  method = c("mean"))
plot(combined_consensus)
dev.copy(png, 'trees\\combinedconsensus.png') 
dev.off()
plot(upgma(combined_mean)) 
dev.copy(png, 'trees\\combinedmean.png') 
dev.off()

# Compare trees -----------------------------------------------------------

genomes_alignment_mt <- Create_Matrix(genomes_matrix, 2, 6)
plot(upgma(genomes_alignment_mt))
dev.copy(png, 'trees\\fivegenomes.png') 
dev.off()
write.table(dists, file = "alignment_matrices\\five_genomes.csv", col.names = TRUE, row.names = TRUE,sep="\t")


# Six virusus -------------------------------------------------------------


genomes_matrix[1] <- "SIX_GENOMES"
genomes_matrix <- c(genomes_matrix, Get_Data_EBOV("Marburg_genome.fasta"))
six_genomes_alignment_mt <- Create_Matrix(genomes_matrix, 2, 7)
plot(upgma(six_genomes_alignment_mt))
dev.copy(png, 'trees\\sixgenomes.png') 
dev.off()
write.table(dists, file = "alignment_matrices\\six_genomes.csv", col.names = TRUE, row.names = TRUE,sep="\t")

# Evolution times -----------------------------------------------------------

# Using dist.dna
l <- list();
i <- 2
while(i < 8) {
  l[[i - 1]] <- t(sapply(strsplit(substring(genomes_matrix[i], 1, 18800),""), tolower))
  i <- i + 1
}
dist_ <- dist.dna(as.DNAbin(l), model = "JC69", gamma = 1000/1.9)
dist_ <- as.matrix(dist_) * 1000 
t <- upgma(dist_)
plot(t)
edgelabels(t$edge.length,  col="black", font=0.25) 
dev.copy(png, 'trees\\distdna.png') 
dev.off() 
# Using edit distances
dists <- matrix(0, 6, 6) 
for (i in 2:7) {
  for (j in 2:7) {   
    dists[i - 1,j - 1] <-   -0.75 * log(1 - 4 / 3 * six_genomes_alignment_mt[i - 1, j - 1]
                                     / nchar(genomes_matrix[i])) * (1000/1.9)
  }
   
}
row.names(dists) <- names_viruses
colnames(dists) <- names_viruses
t <- upgma(dists)
plot(t)
edgelabels(t$edge.length,  col="black", font=0.25)
dev.copy(png, 'trees\\distsJC.png') 
dev.off()
write.table(dists, file = "time_distances\\dists.csv", col.names = TRUE, row.names = TRUE,sep="\t")


 









