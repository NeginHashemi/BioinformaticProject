#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include<bits/stdc++.h>
using namespace std;
//Alignment parameters
const int d = -1;
// int score_[4][4] = {10, -1, -3, -4, -1, 7, -5, -3, -3, -5, 9, 0, -4, -3, 0, 8};
int score[4][4] = {1, -1, -1, -1, -1, 1, -1, -1, -1, -1, 1, -1, -1, -1, -1, 1};
//Hold Marburg genes.
vector <string> genes;
//Hold genes name in order
vector <string> names;
int start_ = 0, end_ = 0;
//Alignment matrix
int mat[8000][20000] = {0};

//Read given path and return content in a string.
string read_from_file(char* path){
	string str = ""; 
	ifstream is(path);
  	char c; 
 	while (is.get(c)){
        if(c == '>') {
        	while(is.get(c)){
        		if(c == '\n') break;
			}
			continue;
		}
   	 	str += c;
	}	
  	is.close(); 
  	return str;
}

//Return maximum of three given numbers.
int max(int a, int b, int c){
  if(a > b){
    if(a > c){
      return a;
    }
    return c;
  }
  if(c > b) return c;
  return b;
}

//Return index of char in score matrix.
int num(char a){
  if(a == 'A') return 0;
  if(a == 'C') return 2;
  if(a == 'G') return 1;
  if(a == 'T') return 3;
  return -1;
}

//Align Marburg given gene to EBOV genome and return gene aligned in EBOV as a string.
string align_(string ebov, string marburg_gene, int es, int lee){
  int mrlen = marburg_gene.length(); 
  //Reset mat
  for (int i = 0; i < 8000; ++i)
  {
    for (int j = 0; j < 20000; ++j)
    {
      mat[i][j] = 0;
    }
  }

  int elen = lee;
  //Fill mat
	for(int i = 0; i < mrlen; i++){
		mat[i][0] = i * d;
	}
  for (int i = 0; i < elen; ++i)
  {
    mat[0][i] = 0;
  }
  int temp; 
  for (int i = 1; i < mrlen; ++i){
    for (int j = 1; j < elen; ++j){
      mat[i][j] = 0;
      temp = mat[i - 1][j - 1] + score[num(marburg_gene[i])][num(ebov[j + es])]; 
      mat[i][j] = max(temp, mat[i - 1][j] + d, mat[i][j - 1] + d); 
    } 
  }

  int max_index = 0;
  int max = INT_MIN;

  for (int i = 0; i < elen; ++i)
  {
    if(mat[mrlen - 1][i] > max) {
      max = mat[mrlen - 1][i];
      max_index = i;
    }
  } 

  //Extract gene string
  string ebov_gene = "";
  int i = mrlen - 1;
  int j = max_index;
  while(i > 0 && j > 0){
    if(i > 0 && j > 0 && mat[i][j] == mat[i-1][j-1] + score[num(marburg_gene[i])][num(ebov[j + es])]){ 
        i--; j--;
    } else if(j > 0 && mat[i][j] == mat[i][j - 1] + d){ 
        j--;
    } else if(i > 0 && mat[i][j] == mat[i - 1][j] + d) i--;
  }
  ebov_gene = ebov.substr(es + j, max_index - j + 1);  
  //Set start_ and end_ to append to file in process method.
  start_ = es + j;
  end_ = es + max_index;
  return ebov_gene;
}

//Read genome from ipath and align each gene to it. Then store results in files.
void process(string ipath, string name, int last){ 
    string genome = read_from_file(&ipath[0u]); 

    ofstream myfile; 

    myfile.open("./gene/start_end_info", std::ios_base::app);

    string gene_1 = align_(genome, genes[1], 0, 4000);
    myfile << name << "1 : " << start_ << " " << end_ << endl; 
    string gene_2 = align_(genome, genes[2], 2000, 4000);
    myfile << name << "2 : " << start_ << " " << end_ << endl; 
    string gene_3 = align_(genome, genes[3], 4000, 4000);
    myfile << name << "3 : " << start_ << " " << end_ << endl; 
    string gene_4 = align_(genome, genes[4], 5500, 4000);
    myfile << name << "4 : " << start_ << " " << end_ << endl; 
    string gene_5 = align_(genome, genes[5], 8300, 4000);
    myfile << name << "5 : " << start_ << " " << end_ << endl; 
    string gene_6 = align_(genome, genes[6], 9700, 4000);
    myfile << name << "6 : " << start_ << " " << end_ << endl; 
    string gene_7 = align_(genome, genes[7], 10990, last);
    myfile << name << "7 : " << start_ << " " << end_ << endl << endl << endl; 

    string path = "./gene/" + name;
    string s;

    s = path + "1.fasta";  
    myfile.open (&s[0u]);
    myfile << "> " << names[0] << endl << gene_1;
    myfile.close();
    s = path + "2.fasta"; 
    myfile.open (&s[0u]);
    myfile << "> " << names[1] << endl << gene_2;
    myfile.close();
    s = path + "3.fasta"; 
    myfile.open (&s[0u]);
    myfile << "> " << names[2] << endl << gene_3;
    myfile.close();
    s = path + "4.fasta"; 
    myfile.open (&s[0u]);
    myfile << "> " << names[3] << endl << gene_4;
    myfile.close();
    s = path + "5.fasta"; 
    myfile.open (&s[0u]);
    myfile << "> " << names[4] << endl << gene_5;
    myfile.close();
    s = path + "6.fasta"; 
    myfile.open (&s[0u]);
    myfile << "> " << names[5] << endl << gene_6;
    myfile.close();
    s = path + "7.fasta"; 
    myfile.open (&s[0u]);
    myfile << "> " << names[6] << endl << gene_7;
    myfile.close();
}


int main() { 
	 
  string str = ""; 
  //Reset file
  ofstream myfile;
  myfile.open("./gene/start_end_info");
  myfile.close();
  //Read genes and store them in "genes" vector.
    ifstream is("/home/negin202/Bio_P/Marburg_Genes.fasta");
   
	char c; 
	string name = "";
 	while (is.get(c)){
        if(c == '>') {
        	genes.push_back(str);
 			str = "";
        	name = "";
        	while(is.get(c)){
        		if(c == '\n') break;
        		name += c;
			}
			names.push_back(name);
			continue;
		}
   	 	str += c;
	}	
	is.close(); 
	genes.push_back(str);  

  //Process data and store in files
  process("/home/negin202/Bio_P/Zaire_genome.fasta", "Zaire_gene_", 8000);
  process("/home/negin202/Bio_P/Bundibugyo_genome.fasta", "Bundibugyo_gene_", 9000);
  process("/home/negin202/Bio_P/Reston_genome.fasta", "Reston_gene_", 9000);
  process("/home/negin202/Bio_P/Sudan_genome.fasta", "Sudan_gene_", 9000);
  process("/home/negin202/Bio_P/TaiForest_genome.fasta", "TaiForest_gene_", 9000);
 
  return 0;
}
